@extends('layouts.app')

@section('content')
    <h2> Product Details </h2>
    <img src="{{ asset('storage/'.$product->image)}} " alt="{{$product->name}}" width="500">
    <ul>
        <li><title>Name: </title> {{ $product->name }}</li>
        <li><title>Price: </title> {{ $product->price }}</li>
        <li><title>Description: </title> {{ $product->description }}</li>
    </ul>
@endsection
