@if($action === 'new')
{!! Form::open(['method' => 'POST', 'route' => 'products.store', 'files' => true]) !!}
@else
{!! Form::model($product, ['method' =>'PATCH', 'route' => ['products.update', $product->id], 'files' => true]) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Product Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Product Price') !!}
    {!! Form::number('price', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Product Description') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    <p>{!! Form::label('image', 'Product Image') !!}</p>
    @if( !empty($product->image))
        <img src="{{ asset('storage/'.$product->image)}} " alt="{{$product->name}}" width="150">
    @endif
    {!! Form::file('image') !!}
</div>
<div class="pull-right">
    {!! Form::submit($action === 'new' ? 'Add' : 'Update', ['class' => 'btn btn-primary']) !!}
</div>

{!! Form::close() !!}
