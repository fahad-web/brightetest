@extends('layouts.app')

@section('content')
    <h2> Create Product </h2>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    @include('products.form', ['action' => 'new'])
@endsection
