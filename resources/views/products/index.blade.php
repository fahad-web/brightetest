@extends('layouts.app')

@section('content')
    <h1> List of Products</h1>

    @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br />
    @endif

    <a class="btn btn-primary" href="{{ route('products.create')}}">Add a New Product</a>
    <table class="table table-bordered">
        <thead>
        <tr>
            <td>Image</td>
            <td>@sortablelink('name')</td>
            <td>@sortablelink('price')</td>
            <td colspan="2">Actions</td>
        </tr>
        </thead>
        @if($products->count())
            @foreach($products as $key => $product)
                <tr>
                    <td>
                        <a href="{{route('products.show',$product->id)}}">
                            <img src="{{ asset('storage/'.$product->image)}}" alt="{{$product->name}}" width="150">
                        </a>
                    </td>
                    <td>
                        <a href="{{route('products.show',$product->id)}}">
                            {{$product->name}}
                        </a>
                    </td>
                    <td>{{$product->price}}</td>
                    <td>
                        <a class="btn  btn-primary" href="{{ route('products.edit', $product->id )}}">Edit</a>
                        {!! Form::model($product, ['method' =>'DELETE', 'route' => ['products.destroy', $product->id], 'onsubmit' => 'return confirm(\'Do you want to delete '.$product->name.'?\')']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
    {!! $products->appends(\Request::except('page'))->render() !!}
@endsection
