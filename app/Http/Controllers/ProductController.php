<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Maximum 10 products per page.
        $products = Product::sortable()->paginate(10);
        return view('products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'required|min:3|max:191',
            'price'                 => 'required|numeric',
            'description'           => 'required',
            'image'                 => 'required|mimes:jpeg,png,gif,jpg',
        ]);

        $image = Storage::disk('public')->put('', $request->image);

        Product::create([
            'name'  => $request->name,
            'price'  => $request->price,
            'description'  => $request->description,
            'image'  => $image
        ]);

        return redirect('products')->with('success', 'The product has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit',compact('product'));
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name'                  => 'required|min:3|max:191',
            'price'                 => 'required|numeric',
            'description'           => 'required',
            'image'                 => 'mimes:jpeg,png,gif,jpg',
        ]);
        $updated_data = [
            'name'  => $request->name,
            'price'  => $request->price,
            'description'  => $request->description,
        ];

        if (!empty($request->image)) {
            $updated_data['image'] = Storage::disk('public')->put('', $request->image);;
        }

        $product->update($updated_data);

        return back()->with('success', 'The product has been update.');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('products')->with('success', 'The product has been deleted');
    }
}
