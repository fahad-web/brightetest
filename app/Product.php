<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Product extends Model
{
    use Sortable;
    protected $fillable = [
        'name',
        'description',
        'price',
        'image',
    ];

    public $sortable = ['name', 'price'];

}
