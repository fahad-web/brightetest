<?php

namespace Tests\Feature;

use App\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    private $name = 'Product 1';
    private $price= '123.00';
    private $description = 'Description for products';

    /**
     * Test all required field validation while creating a new product.
     */
    public function testRequiredValidationDuringCreate() {
        $response = $this->json('POST', '/products',[
        ]);
        $response
            ->assertSee('The name field is required.')
            ->assertSee('The price field is required.')
            ->assertSee('The description field is required.')
            ->assertSee('The image field is required.');
    }

    /**
     *  Test images type validation while creating a new product.
     */
    public function testRequiredImageTypeWhileCreate() {
        Storage::fake('products');

        $response = $this->json('POST', '/products',[
            'image' => UploadedFile::fake()->image('product.jpl')
        ]);

        $response->assertSee('The image must be a file of type: jpeg, png, gif, jpg.');
    }

    /**
     *  Test successful product create.
     */
    public function testSuccessfulCreate() {
        Storage::fake('products');

        $response = $this->json('POST', '/products',[
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'image' => UploadedFile::fake()->image('product.png')
        ]);

        $response
            ->assertRedirect('/products')
            ->assertSessionHas('success', 'The product has been added successfully.');
    }

    /**
     * Test last /products page gets the $products in view and last added products matches the product inserted in the
     * previous test (testSuccessfulCreate()).
     */
    Public function testProductsListing() {
        $response = $this->call('GET', '/products');

        $response
            ->assertViewHas('products');

        $last_added_product = Product::orderBy('ID', 'desc')->first();

        $this->assertTrue(!empty($last_added_product));
        $this->assertEquals($last_added_product['name'], $this->name);
        $this->assertEquals($last_added_product['price'], $this->price);
        $this->assertEquals($last_added_product['description'], $this->description);

    }

    /**
     * Test all required field validation while updating a product.
     */
    public function testRequiredValidationDuringUpdate() {
        $last_added_product = Product::orderBy('ID', 'desc')->first();
        $response = $this->json('PUT', '/products/'.$last_added_product['id'],[
        ]);
        $response
            ->assertSee('The name field is required.')
            ->assertSee('The price field is required.')
            ->assertSee('The description field is required.');
    }

    /**
     *  Test images type validation while updating a new product.
     */
    public function testRequiredImageTypeWhileUpdate() {
        Storage::fake('products');
        $last_added_product = Product::orderBy('ID', 'desc')->first();

        $response = $this->json('PUT', '/products/'.$last_added_product['id'],[
            'image' => UploadedFile::fake()->image('product.jpl')
        ]);

        $response->assertSee('The image must be a file of type: jpeg, png, gif, jpg.');
    }

    /**
     *  Test successful product update.
     */
    public function testSuccessfulUpdate() {
        Storage::fake('products');
        $last_added_product = Product::orderBy('ID', 'desc')->first();

        $response = $this->json('PUT', '/products/'.$last_added_product['id'],[
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'image' => UploadedFile::fake()->image('product.png')
        ]);

        $response
            ->assertSessionHas('success', 'The product has been update.');
    }

    /**
     * Test product delete success.
     */
    public function testSuccessfulDelete() {
        $last_added_product = Product::orderBy('ID', 'desc')->first();

        $response = $this->json('DELETE', '/products/'.$last_added_product['id']);

        $response
            ->assertRedirect('/products')
            ->assertSessionHas('success', 'The product has been deleted');

        $product = Product::find($last_added_product['id']);

        $this->assertTrue(empty($product));
    }

}
