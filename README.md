Brighte Test 
===================
I have worked at 4mation Technology for about 1.5 years and as a Junior Developer and I have had chance to work with latest Laravel Projects.

I have had experience working with few old CakePHP2.4 projects as well.  

That what makes me more comfortable using laravel over cakePHP.

This is the repo for the Brighte Test project. 

Setup
-------------

#### **Environment**

This project utilizes docker for all its local development. It will spin up a workspace, apache2 server and mysql server. 

Before starting the setup make sure you have:
- [docker](https://www.docker.com/)
- Apache and MySQL service are turned off so that docker can use port 80 and 3306. 

#### **Instructions**

_Start Proxy_
```
docker network create -d bridge proxy
docker run -d --name proxy -p 80:80 --network="proxy" -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

_Local Setup_
1. Copy over the env file `cp -n env/.env.local .env`
2. Setup your hosts file. `sudo sh -c 'echo "127.0.0.1       local.brightetest" >> /etc/hosts'`
3. Setup docker containers `docker-compose up -d`
4. SSH into the docker container `./env/mount-workspace.sh`
5. Setup laravel and dependencies `composer install`
6. Run database migration `php artisan migrate`
7. Create public folder symbolic link for public image storage `php artisan storage:link`


Products Page Access
------------------------

Visit page to view product page
http://local.brightetest/products

Testing
-------------

#### **PHPUnit Testing**
Run following command to run the PHPUnit test. 
- `./vendor/bin/phpunit`

Project Development Details
--------------------------------
Project Setup including Docker - 1 hour
CRUD and View setup - 4 hours
Sortable table investigation and setup - 1 hour 
Unit Test - 4 hours
Cleaning up code, readme.md and final testing - 1 hour. 

Total: 11 hours. 

Few shortcuts 
- To create sortable table, i used third party library "kyslik/column-sortable": "^5.7" instead of custom building the feature. 
- Used php artisan command to speed up creating migration, controller for products. 
- I have appropriately used Existing function from model to create CRUD for products. 

Few compromises
- There are currently no feature in place to manage images that are not in used. Images should be deleted if they are not used. 
- The images don't show up while uploaded, it only shows name of the file. I need more time implement proper image display
when uploaded or edited.  

